import React from "react";
import ReactDOM from "react-dom";
import * as serviceWorker from "./serviceWorker";
import { createStore, applyMiddleware, compose } from "redux";
import { Provider } from "react-redux";
import rootReducers from "./reducers";
import Counter from "./components/counter";
import createSagaMiddleware from "redux-saga";
import rootSaga from "./sagas/rootSaga";
import logger from "redux-logger";

const sagaMiddleware = createSagaMiddleware();
const store = createStore(
  rootReducers,
  compose(applyMiddleware(sagaMiddleware, logger), window.devToolsExtension())
);

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Counter />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

sagaMiddleware.run(rootSaga);
serviceWorker.unregister();
