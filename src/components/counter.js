import React from "react";
import { useSelector, useDispatch } from 'react-redux';
import { INCREAMENT, DECREAMENT } from "../actions/actionType";

const Counter = (props) => {
  const time = useSelector(state => state.counterReducers)
  const dispatch = useDispatch();

  return (
    <div>
      <button onClick={() => dispatch({ type: DECREAMENT, step: 1 })}>-</button>
      <button onClick={() => dispatch({ type: INCREAMENT, step: 1 })}>+</button>
      <h1>Count: {time}</h1>
    </div>
  );
};

export default Counter;
