import { INCREAMENT, DECREAMENT } from "./actionType";

export const increamentAction = (step) => {
  return {
    type: INCREAMENT,
    step: step,
  };
};

export const decreamentAction = (step) => {
  return {
    type: DECREAMENT,
    step: step,
  };
};
