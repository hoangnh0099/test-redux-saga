import { INCREAMENT, DECREAMENT } from "../actions/actionType";
import { put, takeEvery, call } from "redux-saga/effects";
import getAPI from "../api";

export function* sayHello() {
  console.log("Hello World");
}

function* increament() {
  console.log("This is increament saga");
}

export function* watchIncreament() {
  yield takeEvery(INCREAMENT, increament);
}

function* decreament() {
  console.log("This is decreament saga");
}

export function* watchDecreament() {
  yield takeEvery(DECREAMENT, decreament);
}

export function* getApiEffect() {
  const data = yield call(getAPI);
  yield put({ type: "API", payload: data.data });
}
