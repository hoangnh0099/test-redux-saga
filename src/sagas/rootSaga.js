import { all } from "redux-saga/effects";
import { sayHello } from "./counter-sagas";
import { watchIncreament, watchDecreament, getApiEffect } from './counter-sagas'

function* rootSaga() {
    yield all([
        sayHello(),
        watchIncreament(),
        watchDecreament(),
    ])
}

export default rootSaga;