import counterReducers from "./counterReducer";
import { combineReducers } from "redux";

const rootReducers = combineReducers({
  counterReducers,
});

export default rootReducers;
