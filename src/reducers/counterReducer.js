import { INCREAMENT, DECREAMENT } from "../actions/actionType";

const counterReducers = (time = 0, action) => {
  switch (action.type) {
    case INCREAMENT:
      return time + action.step;
    case DECREAMENT:
      return time - action.step;
    default:
      return time;
  }
};

export default counterReducers;
