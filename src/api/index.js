import axios from "axios";

const getAPI = async () => {
  return await axios.get("https://jsonplaceholder.typicode.com/posts");
};

export default getAPI;